
#### About This Data:
The dataset provided here is a pet-dataset generated based on partition 3 of the SWAN data benchmark. The data benchmark is openly accessible at [Harvard Dataverse](https://bit.ly/2H02yMH).


Below the architecture of the data directory, as well as the number of multivariate time series (1 per file) per class is illustrated. This pet-dataset contains only 10% of the samples in partition 3. The sampling is done randomly, but the population of samples (on other words, the climatology of flares) is preserved. That is 10% of samples in each class is selected.

```bash
    data
    └── releaseOnHarvard_fold3
        ├── FL
            └── ...
                (|X|= 16, |M| = 115)
            └── ...
        ├── NF
            └── ...
                (|C|= 335, |B| = 10,  |N| = 2222)
            └── ...

```