import pandas as pd
from os import path, walk
import util
from features import extractor_utils


class FeatureExtractor:
    """
    Note: Another version of this class is prepared to be used with multiprocessing. See
    'FeatureExtractorParallel' in 'feature_extractor_parallel.py'.
    This class walks through a directory of csv files of multivariate time series (mvts),
    and for each of them, computes all listed statistical parameters on each of the time series.
    In the mvts files, each column is one time series.

    Key points about the input and output data:

    - Input multivariate time series: 40 X 55 (length of ts, number of ts)
    - Important multivariate time series: 40 X 33 (length of ts, number of important ts)
    - Currently used multivariate time series: 40 X 24
    - Number of currently used physical parameters: 24
    - Number of statistical features: F
    - Number of csv files (slices of time series): N
    - Matrix of all features (df_all_features) extracted from one multivariate timeseries: N X (24*F)

    """

    def __init__(self, path_to_root: str, path_to_dest: str, out_file_name: str):
        """
        This constructor simply initializes the following information to be prepared for
        extracting the statistical features from the physical parameters.

        :param path_to_root: this is where all flaring or non-flaring samples of one fold
        are stored on the server. An example would be:

            - '/data/SHARPS/BERKAY/v0.4/new-data-folds/instances_O12L0P24/fold1/FL/'

        :param path_to_dest: wherever the results are to be stored. If the directory does
        not exist, it will be created recursively. An example would be:
            - '/data/SHARPS/AZIM/v0.4/raw_features_O12L0P24/fold1/'
        :param out_file_name: the name of the output csv file as the calculated data frame. An
        example would be:
            - 'fold1_NF.csv'
        """
        self.path_to_root = path_to_root
        self.path_to_dest = path_to_dest
        self.out_file_name = out_file_name
        self.df_all_features = pd.DataFrame()  # The colnames will be automatically assigned to 'calculate_all'.

    def calculate_all(self,
                      features_list: list,
                      params_name_list: list = None,
                      params_index_list: list = None,
                      need_interp: bool = True):
        """
        Computes all the give statistical features on each of the csv files in 'path_to_root'
        and stores the result in the form of a single csv file.

        :param features_list: list of all selected methods from the module 'feature_collection.py'.
        :param params_name_list: list of the name of physical parameters. Feature extraction will
        be carried out only on the given physical parameters and the remaining parameters will be
        skipped. Use the header row in any of the csv files in SWAN data benchmark to choose from.
        :param params_index_list: similar to 'params_name_list', except that using this, one could
        provide column number instead of column name. Make sure to start from 1 since column 0 is
        has the and it is not a physical parameter. A suggestion would be the range 1:25.
        :param need_interp: True if a linear interpolation is needed to remedy the missing numerical
        values. This only takes care of the missing values and will not affect the existing ones. Set
        to False otherwise. Default is True.
        :return: a csv file where each row corresponds to a time series and represents all the
        extracted features from that time series.
        """
        import sys
        import os

        # -----------------------------------------
        # Verify arguments
        # -----------------------------------------
        has_param_name_arg = (params_name_list is not None) and (len(params_name_list) > 0)
        has_param_index_arg = (params_index_list is not None) and (len(params_index_list) > 0)
        if has_param_name_arg == has_param_index_arg:  # mutual exclusive
            raise ValueError(
                """
                One and only one of the two arguments (params_name_list, params_index_list) must
                be provided.
                """
            )

        if len(features_list) == 0:
            raise ValueError(
                """
                The argument 'features_list' cannot be empty!
                """
            )
        # -----------------------------------------
        # Get all file names in the root directory
        # -----------------------------------------
        print(self.path_to_root)
        dirpath, _, all_csv_files = next(walk(self.path_to_root))
        n = len(all_csv_files)
        n_features = len(features_list)
        i = 1

        print('\n\n\t-----------------------------------'.format())
        print('\t\tTotal No. of Features:\t\t{}'.format(n_features))
        print('\t\tTotal No. of time series:\t{}'.format(n))
        print('\t\tOutput TS dimensionality ({} X {}):\t{}'.format(n, n_features, n * n_features))
        print('\t-----------------------------------\n'.format())

        # -----------------------------------------
        # Loop through each csv file and extract features
        # -----------------------------------------
        for f in all_csv_files:
            print('\t >>> Total Processed: {0} / {1} <<<\r'.format(i, n))
            sys.stdout.flush()

            abs_path = path.join(dirpath, f)
            df_mvts: pd.DataFrame = pd.read_csv(abs_path, sep='\t')

            # -----------------------------------------
            # Keep the requested physical parameters only.
            # -----------------------------------------
            df_raw = pd.DataFrame()
            if has_param_name_arg:
                df_raw = pd.DataFrame(df_mvts[params_name_list], dtype=float)
            elif has_param_index_arg:
                df_raw = pd.DataFrame(df_mvts.iloc[:, params_index_list], dtype=float)

            # -----------------------------------------
            # Interpolate to get rid of the NaN values.
            # -----------------------------------------
            if need_interp:
                df_raw = util.interpolate_missing_vals(df_raw)

            # -----------------------------------------
            # Extract all the features from each column of mvts.
            # -----------------------------------------
            extractedfeatures_df = extractor_utils.calculate_one_mvts(df_raw, features_list)

            # -----------------------------------------
            # Extract some meta data from this mvts.
            # -----------------------------------------
            noaa_no = util.extract_noaa_active_region_number(f)
            flare_class = util.extract_flare_class(f)
            start_time = util.extract_start_time(f)
            end_time = util.extract_end_time(f)

            # -----------------------------------------
            # Flatten the resultant dataframe and add the NOAA AR Number, class label, and start and end times.
            # row_df will then have these columns:
            #   NOAA_AR_NO | LABEL | START_TIME | END_TIME | FEATURE_1 | ... | FEATURE_n
            # -----------------------------------------
            noaa_no_df = pd.DataFrame({'NOAA_AR_NO': [noaa_no]})
            label_df = pd.DataFrame({'LABEL': [flare_class]})
            stime_df = pd.DataFrame({'START_TIME': [start_time]})
            etime_df = pd.DataFrame({'END_TIME': [end_time]})

            features_df = extractor_utils.flatten_to_row_df(extractedfeatures_df)
            row_df = pd.concat([noaa_no_df, label_df, stime_df, etime_df, features_df], axis=1)

            # -----------------------------------------
            # Append this row to 'df_all_features'
            # -----------------------------------------
            # if this is the first file, create the main dataframe, i.e., 'df_all_features'
            if i == 1:
                colnames = list(row_df)
                self.df_all_features = pd.DataFrame(row_df)
            else:
                # add this row to the end of the dataframe 'df_all_features'
                self.df_all_features = self.df_all_features.append(row_df)
            i = i + 1

        print('\n\t{0} files have been processed.'.format(i - 1))
        print('\tAs a result, a dataframe of dimension {} X {} is created.'.format(self.df_all_features.shape[0],
                                                                                   self.df_all_features.shape[1]))
        # -----------------------------------------
        # Store the csv of all features to 'path_to_dest'.
        # -----------------------------------------
        if not os.path.exists(self.path_to_dest):
            os.makedirs(self.path_to_dest)

        fname = os.path.join(self.path_to_dest, self.out_file_name)

        self.df_all_features.to_csv(fname, sep='\t', header=True, index=False)
        print('\n\tThe dataframe is stored at: {0}'.format(fname))

    def calculate_one_mvts(self, df_mvts: pd.DataFrame, features_list: list) -> pd.DataFrame:
        """
        This method computes a list of F statistical features on
        the given multivariate time series of P physical parameters.
        The output is a dataframe (P X F) of the following form:
        -----------------------------------------------
                   min           max          ...
        TOTUSJH    2.828638e+03  3.072033e+03 ...
        TOTBSQ     4.413413e+10  4.625228e+10 ...
        ...        ...           ...          ...
        -----------------------------------------------
        Note: The features will be extracted from all the give columns. So,
        in case it is needed only over some of them, then those columns only
        must be passed in.


        :param df_mvts: the mvts dataframe from which the features are going
        to be extracted.
        :param features_list : list of all functions to be run on the given
        mvts.
        :return: a dataframe with physical parameters as rows and statistical
        features as columns.
        """
        col_names = list(df_mvts)

        # Create an empty dataframe for features to be extracted from this mvts
        df_features = pd.DataFrame(index=col_names, dtype=float)

        # -----------------------------------------
        # Extract all features listed in 'features_list.py' on
        # every column of the given dataframe.
        # -----------------------------------------
        for feature in features_list:
            feature_name = feature.__name__.replace('get_', '')
            df_extracted_feature = df_mvts.apply(feature, axis=0)
            # Add the extracted features to df_features as a column called 'tmp'
            df_features = df_features.assign(tmp=df_extracted_feature.values)
            # Rename the col-name to an appropriate name
            df_features.rename(columns={'tmp': feature_name}, inplace=True)

        return df_features


def main():

    import CONSTANTS as CONST
    import os

    # Prepare data
    path_to_root = os.path.join('..', CONST.IN_PATH_TO_MVTS_NF)
    path_to_dest = os.path.join('..', CONST.OUT_PATH_TO_RAW_FEATURES)
    output_filename = 'raw_features_p3_NF.csv'

    # Prepare two lists, one for the statistical features and another for the physical parameters
    stat_features = CONST.CANDIDATE_STAT_FEATURES
    phys_parameters = CONST.CANDIDATE_PHYS_PARAMETERS

    pc = FeatureExtractor(path_to_root, path_to_dest, output_filename)
    pc.calculate_all(features_list=stat_features, params_name_list=phys_parameters)


if __name__ == '__main__':
    main()
