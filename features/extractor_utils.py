import pandas as pd


def calculate_one_mvts(df_mvts: pd.DataFrame, features_list: list) -> pd.DataFrame:
    """
    This method computes a list of F statistical features on
    the given multivariate time series of P physical parameters.
    The output is a dataframe (P X F) of the following form:
    -----------------------------------------------
               min           max          ...
    TOTUSJH    2.828638e+03  3.072033e+03 ...
    TOTBSQ     4.413413e+10  4.625228e+10 ...
    ...        ...           ...          ...
    -----------------------------------------------
    Note: The features will be extracted from all the give columns. So,
    in case it is needed only over some of them, then those columns only
    must be passed in.


    :param df_mvts: the mvts dataframe from which the features are going
    to be extracted.
    :param features_list : list of all functions to be run on the given
    mvts.
    :return: a dataframe with physical parameters as rows and statistical
    features as columns.
    """
    col_names = list(df_mvts)

    # Create an empty dataframe for features to be extracted from this mvts
    df_features = pd.DataFrame(index=col_names, dtype=float)

    # -----------------------------------------
    # Extract all features listed in 'features_list.py' on
    # every column of the given dataframe.
    # -----------------------------------------
    for feature in features_list:
        feature_name = feature.__name__.replace('get_', '')
        # print('--> {0}'.format(feature_name))
        df_extracted_feature = df_mvts.apply(feature, axis=0)
        # Add the extracted features to df_features as a column called 'tmp'
        df_features = df_features.assign(tmp=df_extracted_feature.values)
        # Rename the col-name to an appropriate name
        df_features.rename(columns={'tmp': feature_name}, inplace=True)

    return df_features


def flatten_to_row_df(df: pd.DataFrame) -> pd.DataFrame:
    """
    For a given dataframe of dimension P X F, where the row names
    (i.e., df's indices) are the physical parameters' names, and
    the column names are the statistical features, this method
    flattens the dataframe into a single-row dataframe of dimension
    1 X (P*F).
    The column names in the resultant dataframe is derived by
    combining the row and column names of the given dataframe.

    :param df: the data frame to be flattened.
    :return: a dataframe with one row and P*F columns.
    """
    # get column names: ['min', 'max', ...]
    all_colnames = list(df)
    # get row names: ['TOTUSJH', 'TOTBSQ', ... ]
    all_rownames = list(df.index)
    # combine them: ['TOTUSJH_min, 'TOTUSJH_max', ... , 'TOTBSQ_min', 'TOTBSQ_max', ...]
    combined_names = [(str(y) + '_' + str(x)) for y in all_rownames for x in all_colnames]
    # flatten the dataframe (row by row)
    row = list(df.values.flatten())
    row_df = pd.DataFrame(columns=combined_names)
    # row_df = pd.DataFrame([row], columns=combined_names) # alternative
    row_df.loc[0] = row
    return row_df


def split(l: list, n_of_partitions: int) -> list:
    """
    Splits the given list l into n_of_paritions partitions of approximately equal size.
    :param l:
    :param n_of_partitions:
    :return: a list of the partitions (lists).
    """
    k, m = divmod(len(l), n_of_partitions)
    return [l[i * k + min(i, m):(i + 1) * k + min(i + 1, m)] for i in range(n_of_partitions)]
