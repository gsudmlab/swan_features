from sklearn.metrics import confusion_matrix

# https://arxiv.org/pdf/1202.5995.pdf
# https://arxiv.org/pdf/1411.1405.pdf

def get_tss(y_true, y_pred, labels=None):
    """
     Calculates the True Skill Score (TSS) based on the true classes and the predicted ones.
     TSS is also called  Hansen-Kuipers Skill Score or Peirce Skill Score.
     For more details, see Bobra & Couvidat (2015), or Bloomfield et al. (2012).

    .. math::

       TSS = (TP / (TP + FN)) - (FP / (FP + TN))

    Note: Keep in mind that the order of the class labels in 'labels'
    defines the positive and negative classes. Here we set it to
    ["CBN", "XM"] which defines "CBN" to be the negative (majority)
    class and "XM" to be the positive (minority) class.
    classes.

    :param y_true: Ground truth (correct) target labels.
    :param y_pred: Predicted labels as returned by a classifier.
    :param labels: List of class labels. First: negative, second:
    positive. Default is set to `["CBN", "XM"]`.

    :return: value of tss.
    """

    if labels is None:
        labels = ["CBN", "XM"]

    tn, fp, fn, tp = confusion_matrix(y_true, y_pred, labels=labels).ravel()

    tp_rate = tp / float(tp + fn) if tp > 0 else 0
    fp_rate = fp / float(fp + tn) if fp > 0 else 0
    return tp_rate - fp_rate


def get_hss1(y_true, y_pred, labels=None):
    """
    Calculates the Heidke Skill Score (HSS) based on the true classes and the predicted ones.
    For more details, see Bobra & Couvidat (2015), or Barnes & Leka (2008).

    .. math::

        HSS1 = (TP + TN - N) / P

    Note: Keep in mind that the order of the class labels in 'labels'
    defines the positive and negative classes. Here we set it to
    ["CBN", "XM"] which defines "CBN" to be the negative (majority)
    class and "XM" to be the positive (minority) class.
    classes.

    :param y_true: Ground truth (correct) target labels.
    :param y_pred: Predicted labels as returned by a classifier.
    :param labels: List of class labels. First: negative, second:
    positive. Default is set to `["CBN", "XM"]`.

    :return: value of hss1.
    """
    if labels is None:
        labels = ["CBN", "XM"]
    tn, fp, fn, tp = confusion_matrix(y_true, y_pred, labels=labels).ravel()
    numer = tp + tn - (fp + tn)
    denom = tp + fn
    return numer / float(denom)


def get_hss2(y_true, y_pred, labels=None):
    """
    Calculates another version of HSS defined by Mason & Hoeksema based on the
    true classes and the predicted ones.
    For more details, see Bobra & Couvidat (2015), or Mason & Hoeksema (2010).

    .. math::

        HSS2 = 2[(TP * TN) - (FN * FP)] / [(P * (FN + TN)] + [(TP + FP) * N)]

    Note: Keep in mind that the order of the class labels in 'labels'
    defines the positive and negative classes. Here we set it to
    ["CBN", "XM"] which defines "CBN" to be the negative (majority)
    class and "XM" to be the positive (minority) class.
    classes.

    :param y_true: Ground truth (correct) target labels.
    :param y_pred: Predicted labels as returned by a classifier.
    :param labels: List of class labels. First: negative, second:
    positive. Default is set to `["CBN", "XM"]`.

    :return: value of hss2.
        """
    if labels is None:
        labels = ["CBN", "XM"]
    tn, fp, fn, tp = confusion_matrix(y_true, y_pred, labels=labels).ravel()
    numer = 2 * ((tp * tn) - (fn * fp))
    denom = ((tp + fn) * (fn + tn)) + ((tp + fp) * (fp + tn))
    return numer / float(denom)
