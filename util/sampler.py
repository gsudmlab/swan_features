import pandas as pd

'''
    This module contains several sampling methods. 
    See ReadMe.md for usage example.
'''

# ------------------------------------------------------------
# ---------------------- UNDERSAMPLING -----------------------
# ------------------------------------------------------------


def undersample_cbn_preserving_climatology(df: pd.DataFrame) -> pd.DataFrame:
    """
    (Undersampling from CBN class by preserving the existing climatology of flares
    among the subclasses.)

    This method undersamples the majority class to reach a 1:1 balance
    between the majority (CBN) and minority (XM) classes. Meanwhile, it
    makes sure that the climatology of the sub-classes is preserved by
    taking two steps:

        1. Keep all X and M instances without any sampling.
        2. Given that |XM| = n, sample w_c * n instances from C-class,
        w_b * n from B-class, and w_n * n from N-class flares, where
        w_c, w_b, and w_n are the fraction of each subclass within their
        group (e.g., w_c = |C| / |CBN|).

    Example (Partition 3):
        BEFORE: |X| = 160, |M| = 1152, |C| = 3350, |B| = 108, |N| = 22236
                |XM| = 1312,  |CBN| = 25694

        AFTER : |X| = 160, |M| = 1152, |C| = 171,  |B| = 5,   |N| = 1135
                |XM| = 1312,  |CBN| = 1311


    :param df: the dataframe on which undersampling is to be performed.
    :return: the undersampled dataframe
    """
    # -------------------------------------
    # split the data into strata
    # -------------------------------------
    df_x = df.loc[df['LABEL'] == 'X']
    df_m = df.loc[df['LABEL'] == 'M']
    df_c = df.loc[df['LABEL'] == 'C']
    df_b = df.loc[df['LABEL'] == 'B']
    df_n = df.loc[df['LABEL'] == 'N']

    # -------------------------------------
    # count class frequencies
    # -------------------------------------
    n_x, n_m, n_c, n_b, n_n = df_x.shape[0], df_m.shape[0], df_c.shape[0], df_b.shape[0], df_n.shape[0]
    n_xm = n_x + n_m
    n_cbn = n_c + n_b + n_n

    # -------------------------------------
    # compute fraction of each subclass
    # -------------------------------------
    w_c = n_c / float(n_cbn)
    w_b = n_b / float(n_cbn)
    w_n = n_n / float(n_cbn)

    # -------------------------------------
    # compute the number of instances to have after undersampling
    # -------------------------------------
    n_cbn_to_have = n_xm  # enforce a 1:1 balance in super-class level
    n_c_to_have = int(w_c * n_cbn_to_have)
    n_b_to_have = int(w_b * n_cbn_to_have)
    n_n_to_have = int(w_n * n_cbn_to_have)

    # -------------------------------------
    # undersample C, B, and N, based on X- and M-class flares
    # -------------------------------------
    sampled_x = df_x
    sampled_m = df_m
    sampled_c = df_c.sample(n_c_to_have)
    sampled_b = df_b.sample(n_b_to_have)
    sampled_n = df_n.sample(n_n_to_have)

    # -------------------------------------
    # concatenate all dataframes into one
    # -------------------------------------
    df_undersampled = pd.concat([sampled_x, sampled_m, sampled_c, sampled_b, sampled_n]).reset_index(drop=True)

    return df_undersampled


def undersample_cbn_enforcing_subclass_balance_by_x(df: pd.DataFrame) -> pd.DataFrame:
    """
    (Undersampling from CBN class by keeping all X class instances, and enforcing
    a 1:1 balance between subclasses X and M in one group, and also C, B, and M in
    the other.)

    This method samples n_x instances from X and n_x instances from M class flares,
    where n_x is the number of X class instances. It then samples (2/3)*n_x instances
    from each of the C, B, and N class flares.

    Note: Due to the fact that X class group forms the smallest class, no oversammpling
    is required to reach a 1:1 balance in the subclass level.

    In short, given |X| = n_x, then:
        * |XM| = |X| + |M| --> n_x + n_x = 2*n_x
        * |CBN| = |C| + |B| + |N| --> (2/3)*n_x + (2/3)*n_x + (2/3)*n_x = 2*n_x

    Example (Partition 3):
        BEFORE: |X| = 160, |M| = 1152, |C| = 3350, |B| = 108, |N| = 22236
                |XM| = 4662, |CBN| = 25694
        AFTER : |X| = 160, |M| = 160,  |C| = 106,  |B| = 106, |N| = 106
                |XM| = 320, |CBN| = 318

    :param df: a dataframe of the numerical values
    :return: the undersampled dataframe
    """
    # -------------------------------------
    # split the data into strata
    # -------------------------------------
    df_x = df.loc[df['LABEL'] == 'X']
    df_m = df.loc[df['LABEL'] == 'M']
    df_c = df.loc[df['LABEL'] == 'C']
    df_b = df.loc[df['LABEL'] == 'B']
    df_n = df.loc[df['LABEL'] == 'N']

    # -------------------------------------
    # count class frequencies
    # -------------------------------------
    n_x, n_m, n_c, n_b, n_n = df_x.shape[0], df_m.shape[0], df_c.shape[0], df_b.shape[0], df_n.shape[0]

    # -------------------------------------
    # compute the number of instances to have after undersampling
    # -------------------------------------
    n_xm_to_have = 2 * n_x  # enforce a 1:1 balance in sub-class level (i.e., |X|=|M|)
    n_cbn_to_have = n_xm_to_have  # enforce a 1:1 balance in super-class level (i.e., |XM|=|CBN|)
    n_c_to_have = int(n_cbn_to_have / 3)  # enforce a 1:1 balance in sub-class level (|C|=|B|=|N|)
    n_b_to_have = int(n_cbn_to_have / 3)  # enforce a 1:1  balance in sub-class level (|C|=|B|=|N|)
    n_n_to_have = int(n_cbn_to_have / 3)  # enforce a 1:1  balance in sub-class level (|C|=|B|=|N|)

    # -------------------------------------
    # undersample based on X-class flare
    # -------------------------------------
    sampled_x = df_x
    sampled_m = df_m.sample(n_x)
    sampled_c = df_c.sample(n_c_to_have)
    sampled_b = df_b.sample(n_b_to_have)
    sampled_n = df_n.sample(n_n_to_have)

    # -------------------------------------
    # concatenate all dataframes into one
    # -------------------------------------
    df_undersampled = pd.concat([sampled_x, sampled_m, sampled_c, sampled_b, sampled_n]).reset_index(drop=True)

    return df_undersampled


def undersample_cbn_enforcing_subclass_balance_by_m(df: pd.DataFrame) -> pd.DataFrame:
    """
    (Undersampling from CBN class by keeping all M class instances, and enforcing
    a 1:1 balance between subclasses X and M in one group, and C, B, and N in the
    other.)

    This method samples n_m instances from X and n_m instances from M class flares,
    where n_m is the number of M-class instances. It then samples (2/3)*n_m instances
    from each of the C-, B-, and N-class flares.

    Note: Due to the fact that M-class group has many more instances than X-class, an
    oversammpling of X class is required to achieve a 1:1 balance in the subclass level.
    This also applies to class B (and C when |C| < |M|), that instead of undersampling,
    oversampling is required.

    In short, given |M| = n_m, then:
        * |XM| = |X| + |M| --> n_m + n_m = 2*n_m
        * |CBN| = |C| + |B| + |N| --> (2/3)*n_m + (2/3)*n_m + (2/3)*n_m = 2*n_m

    Example (Partition 3):
        BEFORE: |X| = 160, |M| = 1152, |C| = 3350, |B| = 108, |N| = 22236
                |XM| = 4662, |CBN| = 25694
        AFTER : |X| = 1152,|M| = 1152, |C| = 768,  |B| = 768, |N| = 768
                |XM| = 2304, |CBN| = 2304

    :param df: a dataframe of the numerical values
    :return: the undersampled dataframe
    """
    # Split the data into strata
    df_x = df.loc[df['LABEL'] == 'X']
    df_m = df.loc[df['LABEL'] == 'M']
    df_c = df.loc[df['LABEL'] == 'C']
    df_b = df.loc[df['LABEL'] == 'B']
    df_n = df.loc[df['LABEL'] == 'N']

    # Count class frequencies
    n_x, n_m, n_c, n_b, n_n = df_x.shape[0], df_m.shape[0], df_c.shape[0], df_b.shape[0], df_n.shape[0]
    n_xm = n_x + n_m
    n_cbn = n_c + n_b + n_n

    # Compute the number of instances to have after undersampling
    n_xm_to_have = 2 * n_m  # enforce a 1:1 balance in sub-class level (i.e., |X|=|M|)
    n_cbn_to_have = n_xm_to_have  # enforce a 1:1 balance in super-class level (i.e., |XM|=|CBN|)
    n_x_to_have = n_m
    n_c_to_have = int(n_cbn_to_have / float(3))  # enforce balance in sub-class level (|C|=|B|=|N|)
    n_b_to_have = int(n_cbn_to_have / float(3))  # enforce balance in sub-class level (|C|=|B|=|N|)
    n_n_to_have = int(n_cbn_to_have / float(3))  # enforce balance in sub-class level (|C|=|B|=|N|)

    # -------------------------------------
    # undersample based on M-class flare
    # -------------------------------------
    extra_x = df_x.sample(n_x_to_have - n_x, replace=True)
    sampled_x = pd.concat([df_x, extra_x])
    sampled_m = df_m
    # sample for C
    if n_c_to_have > n_c:
        extra_c = df_c.sample(n_c_to_have - n_c, replace=True)
        sampled_c = pd.concat([df_c, extra_c])
    else:
        sampled_c = df_c.sample(n_c_to_have)
    # sample for B
    if n_b_to_have > n_b:
        extra_b = df_b.sample(n_b_to_have - n_b, replace=True)
        sampled_b = pd.concat([df_b, extra_b])
    else:
        sampled_b = df_b.sample(n_b_to_have)
    # sample for N
    if n_n_to_have > n_n:  # this never happens. Keeping it just for coherency of code
        extra_n = df_n.sample(n_n_to_have - n_n, replace=True)
        sampled_n = pd.concat([df_n, extra_n])
    else:
        sampled_n = df_n.sample(n_n_to_have)

    # -------------------------------------
    # Concatenate all dataframes into one
    # -------------------------------------
    df_undersampled = pd.concat([sampled_x, sampled_m, sampled_c, sampled_b, sampled_n]).reset_index(drop=True)

    return df_undersampled


# ------------------------------------------------------------
# ---------------------- OVERSAMPLING ------------------------
# ------------------------------------------------------------


def oversample_xm_preserving_climatology(df: pd.DataFrame, suppress_n: bool = False) -> pd.DataFrame:
    """
    (Oversampling from XM class by preserving the existing climatology
    of flares among the subclasses.)

    This method oversamples the minority class to reach a 1:1 balance
    between the minority (XM) and majority (CBN) classes. Meanwhile, it
    makes sure that the climatology of the sub-classes is preserved by
    taking two steps:
        * Keep all C, B, and N instances without any changes.
        * Given that |CBN| = n_c, sample w_x * n_c instances from X-class
        and w_m * n_c from M-class, where w_x and w_m are the fractions of
        each subclass within their group (e.g., w_x = |X| / |XM|).

    Example (Partition 3, suppressed_n=False):
        BEFORE: |X| = 160, |M| = 1152, |C| = 3350, |B| = 108, |N| = 22236
                |XM| = 1312, |CBN| = 25694
        AFTER : |X| = 3133,|M| = 22560,|C| = 3350, |B| = 108, |N| = 22236
                |XM| = 25693, |CBN| = 25694

    Example (Partition 3, suppressed_n=True):
        BEFORE: |X| = 160, |M| = 1152, |C| = 3350, |B| = 108, |N| = 22236
                |XM| = 1312, |CBN| = 25694
        AFTER : |X| = 1225,|M| = 8824,|C| = 3350, |B| = 108, |N| = 6592
                |XM| = 10049, |CBN| = 10050

    :param df: a dataframe of the numerical values
    :param suppress_n: set this to True if you want to reduce the number
    of replicates in the oversampled result by reducing the N-class flares
    before oversampling. If set True, at the very beginning of the process,
    the size of the N-class group will be shrunken down to (n_c * 3) - (n_c + n_b)
    where n_c and n_b are |C| and |B|, respectively. Set this to False, to
    consider the entire N class. Default value is False.
    :return: the overrsampled dataframe
    """
    # -------------------------------------
    # Split the data into strata
    # -------------------------------------
    df_x = df.loc[df['LABEL'] == 'X']
    df_m = df.loc[df['LABEL'] == 'M']
    df_c = df.loc[df['LABEL'] == 'C']
    df_b = df.loc[df['LABEL'] == 'B']
    df_n = df.loc[df['LABEL'] == 'N']

    # -------------------------------------
    # Count class frequencies
    # -------------------------------------
    n_x, n_m, n_c, n_b, n_n = df_x.shape[0], df_m.shape[0], df_c.shape[0], df_b.shape[0], df_n.shape[0]

    if suppress_n:
        n_of_supressed_n = (n_c * 3) - (n_c + n_b)
        df_n = df_n.sample(n_of_supressed_n)
        n_n = df_n.shape[0]

    n_xm = n_x + n_m
    n_cbn = n_c + n_b + n_n

    # -------------------------------------
    # Compute fraction of each subclass
    # -------------------------------------
    w_x = n_x / float(n_xm)
    w_m = n_m / float(n_xm)

    # -------------------------------------
    # Compute the number of instances to have after oversampling
    # -------------------------------------
    n_xm_to_have = n_cbn
    n_x_to_have = int(w_x * n_xm_to_have)
    n_m_to_have = int(w_m * n_xm_to_have)

    # -------------------------------------
    # Oversample based on N-class flares
    # -------------------------------------
    sampled_x = df_x.sample(n_x_to_have, replace=True)
    sampled_m = df_m.sample(n_m_to_have, replace=True)
    sampled_c = df_c
    sampled_b = df_b
    sampled_n = df_n

    # -------------------------------------
    # Concatenate all dataframes into one
    # -------------------------------------
    df_oversampled = pd.concat([sampled_x, sampled_m, sampled_c, sampled_b, sampled_n]).reset_index(drop=True)

    return df_oversampled


def oversample_xm_enforcing_subclass_balance_by_c(df: pd.DataFrame) -> pd.DataFrame:
    """
    (Oversampling from XM class by keeping all C class instances, and enforcing
    a 1:1 balance between subclasses X and M in one group, and also C, B, and N in
    the other.)

    This method samples n_c instances from each of the C, B, and N classes, where n_c
    is the number of C class instances. It then samples (3/2) * n_c instances from each
    of the X and M class flares.

    Note: Due to the fact that C-class group very often has many more instances than B class,
    in addition to oversampling in X and M category, B class, from CBN category, also requires oversampling
    to achieve a 1:1 balance in the subclass level.

    Note: Since the population of instances in N class is much larger than that in C,
    an undersampling is required to achieve a 1:1 balance in the subclass level.

    In short, given |C| = n_c, then after oversampling:

        * |XM| = |X| + |M| --> (3/2) * n_c + (3/2) * n_c = 3 * n_c
        * |CBN| = |C| + |B| + |N| --> n_c + n_c + n_c = 3 * n_c

    Example (Partition 3):
        BEFORE: |X| = 160, |M| = 1152, |C| = 3350, |B| = 108, |N| = 22236
                |XM| = 1312,  |CBN| = 25694
        AFTER : |X| = 5025,|M| = 5025, |C| = 3350, |B| = 3350,|N| = 3350
                |XM| = 10050, |CBN| = 10050

    :param df: a dataframe of the numerical values
    :return: the over-sampled results in the form of data (first) and labels (second)
    with the similar structure as the input dataframes.
    """
    # -------------------------------------
    # split the data into strata
    # -------------------------------------
    df_x = df.loc[df['LABEL'] == 'X']
    df_m = df.loc[df['LABEL'] == 'M']
    df_c = df.loc[df['LABEL'] == 'C']
    df_b = df.loc[df['LABEL'] == 'B']
    df_n = df.loc[df['LABEL'] == 'N']

    # -------------------------------------
    # count class frequencies
    # -------------------------------------
    n_x, n_m, n_c, n_b, n_n = df_x.shape[0], df_m.shape[0], df_c.shape[0], df_b.shape[0], df_n.shape[0]

    # -------------------------------------
    # Compute the number of instances to have after oversampling
    # -------------------------------------
    n_cbn_to_have = 3 * n_c
    n_x_to_have = int(n_cbn_to_have / float(2))
    n_m_to_have = int(n_cbn_to_have / float(2))
    n_b_to_have = n_c  # enforce balance in sub-class level (|C|=|B|=|N|)
    n_n_to_have = n_c  # enforce balance in sub-class level (|C|=|B|=|N|)

    # -------------------------------------
    # Oversample based on C class flare
    # -------------------------------------
    # sample for M
    if n_m_to_have > n_m:
        extra_m = df_m.sample(n_m_to_have - n_m, replace=True)
        sampled_m = pd.concat([df_m, extra_m])
    else:
        sampled_m = df_m.sample(n_m_to_have)

    # sample for X
    if n_x_to_have > n_x:  # this never happens. Keeping it just for coherency of code
        extra_x = df_x.sample(n_x_to_have - n_x, replace=True)
        sampled_x = pd.concat([df_x, extra_x])
    else:
        sampled_x = df_x.sample(n_x_to_have)

    # sample for C
    sampled_c = df_c

    # sample for B
    if n_b_to_have > n_b:
        extra_b = df_b.sample(n_b_to_have - n_b, replace=True)
        sampled_b = pd.concat([df_b, extra_b])
    else:
        sampled_b = df_b.sample(n_b_to_have)

    # sample for N
    if n_n_to_have > n_n:  # this never happens. Keeping it just for coherency of code
        extra_n = df_n.sample(n_n_to_have - n_n, replace=True)
        sampled_n = pd.concat([df_n, extra_n])
    else:
        sampled_n = df_n.sample(n_n_to_have)

    # -------------------------------------
    # Concatenate all dataframes into one
    # -------------------------------------
    df_oversampled = pd.concat([sampled_x, sampled_m, sampled_c, sampled_b, sampled_n]).reset_index(drop=True)

    return df_oversampled


def oversample_xm_enforcing_subclass_balance_by_n(df: pd.DataFrame) -> pd.DataFrame:
    """
    (Oversampling from XM class by keeping all N class instances, and enforcing
    a 1:1 balance between subclasses X and M in one group, and also C, B, and N in
    the other.)

    This method samples n_n instances from each of the C, B, and N classes, where n_n
    is the number of N class instances. It then samples (3/2)*n_n instances from each
    of the X and M class flares.

    Note: Due to the fact that N class group always has more instances than any other
    class, in addition to oversampling X and M classes, C and B classes would also
    require oversampling to achieve a 1:1 balance in the subclass level.

    In short, given |N| = n_n, then after oversampling:
        * |XM| :: |X| + |M| :: (3/2) * n_c + (3/2) * n_c :: 3 * n_c
        * |CBN| :: |C| + |B| + |N| :: n_c + n_c + n_c :: 3 * n_c

    Example (Partition 3):
        BEFORE: |X| = 160,  |M| = 1152,  |C| = 3350,  |B| = 108,  |N| = 22236
                |XM| = 4662, |CBN| = 25694
        AFTER : |X| = 33354,|M| = 33354, |C| = 22236, |B| = 22236,|N| = 22236
                |XM = 66708, |CBN| = 25694
    :param df: a dataframe of the numerical values
    :return: the oversampled dataframe
    """
    # split the data into strata
    df_x = df.loc[df['LABEL'] == 'X']
    df_m = df.loc[df['LABEL'] == 'M']
    df_c = df.loc[df['LABEL'] == 'C']
    df_b = df.loc[df['LABEL'] == 'B']
    df_n = df.loc[df['LABEL'] == 'N']

    # count class frequencies
    n_x, n_m, n_c, n_b, n_n = df_x.shape[0], df_m.shape[0], df_c.shape[0], df_b.shape[0], df_n.shape[0]

    # compute the number of instances to have after oversampling
    n_cbn_to_have = 3 * n_n

    n_x_to_have = int(n_cbn_to_have / 2)
    n_m_to_have = int(n_cbn_to_have / 2)
    n_c_to_have = n_n  # enforce balance in sub-class level (|C|=|B|=|N|)
    n_b_to_have = n_n  # enforce balance in sub-class level (|C|=|B|=|N|)

    # -------------------------------------
    # oversample based on N class flare
    # -------------------------------------
    # sample for M
    if n_m_to_have > n_m:
        extra_m = df_m.sample(n_m_to_have - n_m, replace=True)
        sampled_m = pd.concat([df_m, extra_m])
    else:
        sampled_m = df_m.sample(n_m_to_have)

    # sample for X
    if n_x_to_have > n_x:  # this never happens. Keeping it just for coherency of code
        extra_x = df_x.sample(n_x_to_have - n_x, replace=True)
        sampled_x = pd.concat([df_x, extra_x])
    else:
        sampled_x = df_x.sample(n_x_to_have)

    # sample for C
    if n_c_to_have > n_c:
        extra_c = df_c.sample(n_c_to_have - n_c, replace=True)
        sampled_c = pd.concat([df_c, extra_c])
    else:
        sampled_c = df_b.sample(n_c_to_have)

    # sample for B
    if n_b_to_have > n_b:
        extra_b = df_b.sample(n_b_to_have - n_b, replace=True)
        sampled_b = pd.concat([df_b, extra_b])
    else:
        sampled_b = df_b.sample(n_b_to_have)

    # sample for N
    sampled_n = df_n

    print(sampled_x.shape)
    print(sampled_m.shape)
    print(sampled_c.shape)
    print(sampled_b.shape)
    print(sampled_n.shape)
    # -------------------------------------
    # concatenate all dataframes into one
    # -------------------------------------
    df_oversampled = pd.concat([sampled_x, sampled_m, sampled_c, sampled_b, sampled_n]).reset_index(drop=True)

    return df_oversampled

