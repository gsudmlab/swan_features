import pandas as pd


def show_general_info(df: pd.DataFrame):
    print('\n')
    print('\t\tshape:\t{} X {}'.format(df.shape[0], df.shape[1]))
    print('---------------- GENERAL INFO ----------------')
    df.info()
    print('----------------------------------------------')


def show_na_stats(df: pd.DataFrame, count_threshold: int = 1, only_return: bool = False):
    """
    Counts all None, NaN, NaT, and optionally numpy.inf (depending on pandas.options.mode.use_inf_as_na)
    of the class dataframe. This is done by counting all non-NA cells for each column and subtract them
    from the column length.

    :count_threshold: an integer indicating the minimum number of NA's for a column to have to be
    taken into account when reporting missing values. The default value is 1, indicating that any
    column with at least one NA record will be reported.

    :only_return: if False (default value), then it prints the resulting dataframe in addition
    to returning it. If True, it does not print anything.

    :return: a dataframe with four columns, namely 'COLUMN_INDEX', 'COLUMN_NAME', 'NA_COUNT', and
    'NA_PERCENT', keeps track of the number of missing values in each column.

    """
    col_length = df.shape[0]
    row_length = df.shape[1]
    cols_na_count = len(df) - df.count(axis=0)  # type:pd.Series
    res_df = pd.DataFrame(columns=["COLUMN_INDEX", "COLUMN_NAME", "NA_COUNT", "NA_PERCENT"])
    i = 0
    j = 0
    total_missing_counts = 0
    for col_na_count in cols_na_count:
        if col_na_count >= count_threshold:
            total_missing_counts = total_missing_counts + col_na_count
            missing_percent = str(round(float(col_na_count * 100) / col_length, 2))
            res_df.loc[j] = [i, cols_na_count.index[i], col_na_count, missing_percent]
            j = j + 1
        i = i + 1
    total_missing_percent = str(round(float(total_missing_counts * 100) / (row_length * col_length), 4))
    if not only_return:
        print('\n---------------- NA REPORT -------------------')
        print(res_df)
        print('----------------------------------------------')
        print('\tIn Total (with threshold = {0}): \t{1}% NAs.'.format(count_threshold, total_missing_percent))
        print('----------------------------------------------')

    return res_df


def main():
    in_path = '/home/azim/SHARPS/AZIM/v1.2/raw_features_O12L0P24/fold3/fold3_FL_Parallel_with_NA.csv'
    df = pd.read_csv(in_path, sep='\t')
    show_general_info(df)
    res_df = show_na_stats(df, 1, False)


if __name__ == "__main__":
    main()
