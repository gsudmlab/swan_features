import os
import numpy as np


def extract_noaa_active_region_number(file_name):
    """
    Extracts the NOAA Active Region number from the MVTS file file name
    by splitting the name by '_', taking the second part, dropping the
    'ar' and using the rest as the AR number.

    For instance, a file with the following name:
        'M1.0@265_ar115_s2010-08-07T02:36:00_e2010-08-07T14:24:00.csv'
    would first be split into 4 parts:
        'M1.0@265', 'ar115', 's2010-08-07T02:36:00', 'e2010-08-07T14:24:00.csv'
    and by skipping the first 2 characters of the second part, the NOAA AR Number
    would be 115.
    :param file_name: name of the csv file having a MVTS data. This can be just
    the file name, relative path, or absolute path.
    :return: an integer representing the NOAA AR Number corresponding to this
    MVTS.
    """
    file_name = os.path.basename(os.path.normpath(file_name))
    noaa_no = np.int(file_name.split('_')[1][2:])
    return noaa_no


def extract_start_time(file_name):
    """
    Extracts the start time from a given file name, by taking the substring after "s"

    For instance, for files in NF directory:
        B1.0@1053_ar345_s2011-01-24T03:24:00_e2011-01-24T11:12:00.csv     --> 2011-01-24T03:24:00
        NF_ar650_s2011-06-06T11:12:00_e2011-06-06T19:00:00.csv            --> 2011-06-06T11:12:00
    and for files in FL directory:
        M6.6@1175_ar377_s2011-02-12T20:00:00_e2011-02-13T03:48:00.csv     --> 2011-02-12T20:00:00

    :param file_name: name of the csv file having a MVTS data. This can be just
    the file name, relative path, or absolute path.
    :return: the end time as a string (format --> YYYY-MM-DDThh:mm:ss).
    """
    file_name = os.path.basename(os.path.normpath(file_name))
    return file_name.strip('.csv').split('_')[2].lstrip('s')


def extract_end_time(file_name):
    """
    Extracts the end time from a given file name, by taking the substring after "e"

    For instance, for files in NF directory:
        B1.0@1053_ar345_s2011-01-24T03:24:00_e2011-01-24T11:12:00.csv     --> 2011-01-24T11:12:00
        NF_ar650_s2011-06-06T11:12:00_e2011-06-06T19:00:00.csv            --> 2011-06-06T19:00:00
    and for files in FL directory:
        M6.6@1175_ar377_s2011-02-12T20:00:00_e2011-02-13T03:48:00.csv     --> 2011-02-13T03:48:00

    :param file_name: name of the csv file having a MVTS data. This can be just
    the file name, relative path, or absolute path.
    :return: the end time as a string (format --> YYYY-MM-DDThh:mm:ss).
    """
    file_name = os.path.basename(os.path.normpath(file_name))
    return file_name.strip('.csv').split('_')[3].lstrip('e')


def extract_flare_class(file_name):
    """
    Extracts the flare class from the given file name, by taking the first letter
    as the indication. (Existing classes are: B, C, M, and X)

    For instance, for files in NF directory:
        B1.0@1053_ar345_s2011-01-24T03:24:00_e2011-01-24T11:12:00.cs      --> B
        NF_ar650_s2011-06-06T11:12:00_e2011-06-06T19:00:00.csv            --> NF
    and for files in FL directory:
        M6.6@1175_ar377_s2011-02-12T20:00:00_e2011-02-13T03:48:00.csv     --> M
        X2.2@1194_ar377_s2011-02-14T01:00:00_e2011-02-14T08:48:00.csv     --> X

    :param file_name: name of the csv file having a MVTS data. This can be just
    the file name, relative path, or absolute path.
    :return: the flare class as a one-char string.
    """
    file_name = os.path.basename(os.path.normpath(file_name))
    return file_name[0]
