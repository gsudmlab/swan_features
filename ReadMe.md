----
#### Requirements

* Python 3.6
* For a list of all required packages, see [requirements.txt](requirements.txt).

----


## Multivariate Timeseries Feature Extraction on SWAN Data Benchmark
This module aims to simplify the feature extraction process for those interested in working with SWAN data benchmark. The main objectives of this module are as follows.

This module provides:

1. a set of useful statistical features to be extracted form the multivariate time series, in order to transform the time series dataset into tabular data.
2. means to facilitate extraction of the features from a high dimensional multivariate time series dataset. 
3. several sampling methodologies tailored specifically for flare forecasting problem, to remedy the class-imbalance issue which is intrinsic to any rare-event forecast problem like the one SWAN targets to benchmark.
4. multiple normalization methods that can be used and potentially improve a forecast model's performance.
5. several performance metrics that several studies have showen to be effective in reflecting forecast models' performance.

In the following, we briefly introduce the dataset, all the means provided by this module, and some snippets of code to showcase how the module can be used.


### SWAN (Space Weather ANalytics) Data Benchmark
SWAN [1] is a comprehensive, multivariate time series (MVTS) dataset extracted from solar photospheric vector magnetograms in Spaceweather HMI Active Region Patch (SHARP) series. This openly accessible data benchmark includes a cross-checked NOAA solar flare catalog that immediately facilitates solar flare prediction efforts.

A small dataset comprised of 10% of partition 3 of SWAN data benchmark, comes with this project and can be found at [./data/input/time_segmented_partition3_subset](data/input/time_segmented_partition3_subset). In the figure below, population of each class (which is 10% of the population of flares in partition 3) is illustrated.


![figures/flare_population_subpartition3.svg](figures/flare_population_subpartition3.svg)

`|X| = 16`, `|M| = 115`, `|C| = 335`, `|B| = 10`, `|N| = 2222`

### FEATURES PACKAGE [[features](features)]
This package contains the statistical features that can be extracted from the time series, and the scripts needed to compute the features on the multivariate time series of SWAN data benchmark.

#### Features Collection [[features.feature_collection.py](features/feature_collection.py)]
A collection of 50 time series statistical features is available
in this script.

The chosen statistical features are listed belwo. In general, these *50* features can be grouped into nine clusters of descriptors. In the **1st** and **2nd** groups, there are the descriptive statistics (e.g., *min*, *max*, etc) to describe the time series in their entirety, and to compare their first and second halves, respectively. The **3rd** group contains the representation of the time series in terms of their extrema (e.g., *number of local minima*, *average of local minima*). We also include several derivative-based features using the difference-based derivative, and form the **4th** group. The **5th** group is the same the 4th, except that gradient-based derivative is used. Another collection, the **6th** group, is wrapped around those features which quantify the high-level changes within the time series (e.g., *average absolute change*). The **7th** group describes the positive and negative fractions of the time series, and disregards the temporal aspect of the time series. The features in the **8th** group focus on the tail of the time series (e.g., *last value*). And finally, the **9th** cluster contains several features describing the long-run trends of the time series (e.g., *slope of longest monotonic increase*).

---

* **GROUP 1**: [`min`, `max`, `median`, `mean`, `stddev`, `var`, `skewness`, `kurtosis`]
* **GROUP 2**: [`difference_of_mins`, `difference_of_maxs`, `difference_of_medians`, `difference_of_stds`, `difference_of_vars`, `difference_of_skewness`, `difference_of_kurtosis`]
* **GROUP 3**: [`no_local_maxima`, `no_local_minima`, `no_local_extrema`, `no_zero_crossings`, `mean_local_maxima_value`, `mean_local_minima_value`]
* **GROUP 4**: [`dderivative_mean`, `dderivative_stddev`, `dderivative_skewness`, `dderivative_kurtosis`]
* **GROUP 5**: [`gderivative_mean`, `gderivative_stddev`, `gderivative_skewness`, `gderivative_kurtosis`]
* **GROUP 6**: [`linear_weighted_average`, `quadratic_weighted_average`, `average_absolute_change`, `average_absolute_derivative_change`]
* **GROUP 7**: [`positive_fraction`, `negative_fraction`]
* **GROUP 8**: [`last_value`, `last_K`, `sum_of_last_K`, `mean_last_K`]
* **GROUP 9**: [`longest_positive_run`, `longest_negative_run`, `longest_monotonic_increase`, `longest_monotonic_decrease`,  `slope_of_longest_mono_increase`, `slope_of_longest_mono_decrease`, `avg_mono_increase_slope`, `avg_mono_decrease_slope`]

---


#### Features Extractor (sequential) [[feature_extractor.py](features/feature_extractor.py)]
This extracts statistical features from the multivariate time series in SWAN data benchmark. Using this script, any of the features mentioned above can be selected to be computed on any subset of the physical parameters in SWAN.

The snippet below shows how to extract 3 statistical features (i.e., `min`, `max`, and `median`) from 4 physical parameters (i.e., `TOTUSJH`, `TOTBSQ`, `TOTPOT`, `TOTUSJZ`). The extracted features will be a `pandas.DataFrame` (stored as a csv file) with its shape being `N X M`, where `N` is the number of multivaraite time series available in `/path/to/FL/`, and `M` is the dimensionality of each extracted vector. In this exmaple, `M` equals the number of generated features (3*4), plus 4; four pieces of meta data, `NOAA_AR_NO`, `LABEL`, START_TIME`, and `END_TIME`.

```python
import os
import CONSTANTS as CONST
import features.feature_collection as fc
from features.feature_extractor import FeatureExtractor

# Input and output path
path_to_root = os.path.join('..', CONST.IN_PATH_TO_MVTS_FL)
path_to_dest = os.path.join('..', CONST.OUT_PATH_TO_RAW_FEATURES)
output_filename = 'raw_features_p3_FL.csv'

# Choose statistical features and physical parameters
features_list = [fc.get_min, fc.get_max, fc.get_median]
params_name_list = ['TOTUSJH', 'TOTBSQ', 'TOTPOT', 'TOTUSJZ']
# Extract features
pc = FeatureExtractor(path_to_root, path_to_dest, output_filename)
pc.calculate_all(features_list, params_name_list=params_name_list)
```

#### Features Extractor (parallel) [[feature_extractor_parallel.py](features/feature_extractor_parallel.py)]

This outputs the same results as [feature_extractor.py](features/feature_extractor.py), except that this runs in parallel using [multiprocessing](https://docs.python.org/3.6/library/multiprocessing.html) library to speed up the feature extraction process. Usage of this script is specially favorable when a large list of statistical features are to be extracted from all physical parameters.

Note: While all processes should have access to the multivariate time series, data do not need to be replicated separately for each process. The snippet below show how the address to the data (and not the data themselves) is partitioned and distributed among the processes.

```python
import os
import pandas as pd
from os import path, walk
import CONSTANTS as CONST
import multiprocessing as mp
from features import extractor_utils, FeatureExtractorParallel

n_procs = 4  # total number of processes
stat_features = CONST.CANDIDATE_STAT_FEATURES
phys_parameters = CONST.CANDIDATE_PHYS_PARAMETERS

# Prepare data
path_to_root = os.path.join('..', CONST.IN_PATH_TO_MVTS_FL)
path_to_dest = os.path.join('..', CONST.OUT_PATH_TO_RAW_FEATURES)
output_filename = 'raw_features_p3_FL_parallel.csv'

# Collect all files (only the absolute paths)
all_files = list()
dirpath, _, all_csv = next(walk(path_to_root))
for f in all_csv:
    abs_path = path.join(dirpath, f)
    all_files.append(abs_path)
    # partition the files to be distributed among processes.
    partitions = extractor_utils.split(all_files, n_procs)
    # Assign a partition to each process
    proc_id = 0
    manager = mp.Manager()
    extracted_features = manager.list()
    jobs = []
    # Assign the partitions to processes
    for partition in partitions:
        pc = FeatureExtractorParallel(features_list=stat_features,
                                      params_name_list=phys_parameters,
                                      params_index_list=None,
                                      need_interp=True)
        process = mp.Process(target=pc.calculate_all,
                             args=(proc_id, partition, extracted_features))
        jobs.append(process)
        proc_id = proc_id + 1
    # start all the processes
    for j in jobs:
        j.start()
    # wait for all the processes to finish
    for j in jobs:
        j.join()

    # Create a dataframe of all features extracted by each process
    all_extracted_features = pd.concat(extracted_features)

``` 

### UTIL PACKAGE [[util](util)]
This package, in addition to containing a couple of helper methods, provides means for data sampling and normalization.

#### Normalizer [[util.normalizer.py](util/normalizer.py)]
Several data normalizer methods are provided in this script, which are tailored specifically for normalization of the extracted features from SWAN data benchmark.

The snippet below shows usage of `zeroOneNormalizer` on the features extracted from *fold3* of SWAN. Note that if multiple partitions are to be used, they must be combined first and then fed to the normalizer all at once. Otherwise, normalization will take place using the local extrema.

```python
import pandas as pd
import util.normalizer as normalizer
# load and combine the flaring (FL) and non-flaring (NF) mvts data.
path_to_fl = '/path/to/extractedFeatures_FL.csv'
path_to_nf = '/path/to/extractedFeatures_NF.csv'
path_to_dest = '/path/to/destination/'
output_filename = 'extractedFeatures_normalized.csv' 
# combine FL and NF data
d_fl = pd.read_csv(path_to_fl, sep='\t')
d_nf = pd.read_csv(path_to_nf, sep='\t')
d_combined = pd.concat([d_fl, d_nf], ignore_index=True)

# provide a list of all non-numerical columns to be excluding
excluded_cols = ['NOAA_AR_NO', 'LABEL', 'START_TIME', 'END_TIME']
# normalize
df_norm = normalizer.zero_one_normalize(d_combined, excluded_cols)
```

#### Sampler [[util.sampler.py](util/sampler.py)]

SWAN data benchmark is shown to be extremely imbalanced. The plot below illustrates this issue very well.

![figures/5_class_population.svg](figures/5_class_population.svg)

One of the wellknown remedies for class-imbalance issue is to achieve a 1:1 balance using undersampling and/or oversampling. This script provides several sampling methods for changing the population of instances of minority classes (`X` or `M`) and majority classes (`C`, `B`, or `N`). While sampling can target these 5 classes, they could also balance the dataset only on the superclass level (i.e., `XM` and `CBN`).

The methods are as follows:

|i   | method                                  |   Description    |
|:---|:----------------------------------------|:-----------------|
|1|`undersample_cbn_preserving_climatology`  | undersamples from the majority class, while preserve the climatology of `C`-, `B`-, and `N`-classe flares. |
|2|`undersample_cbn_enforcing_subclass_balance_by_x`| undersamples from all classes except `X`, such that the dataset is balanced both in the superclass level and subclass level. |
|3|`undersample_cbn_enforcing_subclass_balance_by_m`| undersamples from `CBN` class by keeping all `M`-class instances, and enforcing   a 1:1 balance between subclasses `X` and `M` in one group, and `C`, `B`, and `N` in the other.|
|4|`oversample_xm_preserving_climatology`| Oversamples from `XM` class by preserving the existing climatology of flares among the subclasses. This is comparable to 1.|
|5|`oversample_xm_enforcing_subclass_balance_by_c`| Oversamples from `XM` class by keeping all `C` class instances, and enforcing a 1:1 balance between subclasses `X` and `M` in one group, and also `C`, `B`, and `N` in the other. This is comparable to 3.|
|6|`oversample_xm_enforcing_subclass_balance_by_n`| Oversamples from `XM` class by keeping all `N` class instances, and enforcing a 1:1 balance between subclasses `X` and `M` in one group, and also `C`, `B`, and `N` in the other. This is comparable to 3.| 


#### Metrics [[util.metrics.py](util/metrics.py)]
The commonly used performance metrics (e.g. accuracy, precision & recall) are very likely to obscure the real performance of a forecasting model when we deal with imbalance datasets. On the other hand, regardless of the remedy we employ for this issue (e.g., undersampling, oversampling, class weights, etc), the test phase is always carried out on the test set without any changes to the class frequencies. Therefore the performance metric of choice must be insensitive to the class-imbalance issue. In this module, some of the most popular measures used in flare-forecast problem (also popular in weather forecast domain) are implemented. Table below encapsulates these methods.



| Method |  Formula    |Description                   | Reference  |
|:-------|:------------|:-----------------------------|:-----------|
| TSS    | TSS = (TP / (TP + FN)) - (FP / (FP + TN))  | Calculates the True Skill Score (TSS) based on the true classes and the predicted ones. TSS is also called *Hansen-Kuipers Skill Score* or *Peirce Skill Score.* |  *Bobra* & *Couvidat* (2015) [2], *Bloomfield* et al. (2012) [3]|
| HSS1    | HSS1 = (TP + TN - N) / P  | Calculates the Heidke Skill Score (HSS) based on the true classes and the predicted ones. |  *Bobra* & *Couvidat* (2015) [2], or *Barnes* & *Leka* (2008) [5]|
| HSS2    | HSS2 = 2[(TP * TN) - (FN * FP)] / [(P * (FN + TN)] + [(TP + FP) * N)]  | Calculates another version of HSS defined by *Mason* & *Hoeksema* based on the true classes and the predicted ones. |  *Bobra* & *Couvidat* (2015) [2], or *Mason* & *Hoeksema* (2010) [4]|


#### Meta Data Getter [[util.meta_data_getter.py](util/meta_data_getter.py)]
All meta data describing each multivariate time series is encoded in its file name. As an example, consider a multivariate time series data corresponding to a B-class flare that is named `B1.0@1053_ar345_s2011-01-24T03:24:00_e2011-01-24T11:12:00.csv`. The name indicates that the *start time* of this observation window is `2011-01-24T03:24:00`, the *end time* is `2011-01-24T11:12:00`, the corresponding active region is indexed by NOAA as `345`, which is a `B`-class flare. Similarly, we use the following function to extract such information:

```python
>>> fname = 'path/to/B1.0@1053_ar345_s2011-01-24T03:24:00_e2011-01-24T11:12:00.csv'
>>> import util.meta_data_getter as mgetter
>>> mgetter.extract_noaa_active_region_number(fname)
345
>>> mgetter.extract_start_time(fname)
'2011-01-24T03:24:00'
>>> mgetter.extract_end_time(fname)
'2011-01-24T11:12:00'
>>> mgetter.extract_flare_class(fname)
'B'
```

The above methods are called during the feature extraction process, i.e., in [feature_extractor.py](features/feature_extractor.py) and [feature_extractor_parallel.py](features/feature_extractor_parallel.py).


----
####References:

[1] Aydin, Berkay and others, "Manuscript submitted for publication, Scientific Data", 2019, *Manuscript submitted for publication, Scientific Data*, data available at [Harvard Dataverse](https://bit.ly/2H02yMH)

[2] Bobra, Monica G., and Sebastien Couvidat. "Solar flare prediction using SDO/HMI vector magnetic field data with a machine-learning algorithm." *The Astrophysical Journal* 798.2 (2015): 135. [https://arxiv.org/pdf/1411.1405.pdf](https://arxiv.org/pdf/1411.1405.pdf)

[3] Bloomfield, D. Shaun, et al. "Toward reliable benchmarking of solar flare forecasting methods." *The Astrophysical Journal Letters* 747.2 (2012): L41. [https://arxiv.org/pdf/1202.5995.pdf](https://arxiv.org/pdf/1202.5995.pdf)

[4] Mason, J. P., and J. T. Hoeksema. "Testing automated solar flare forecasting with 13 years of Michelson Doppler Imager magnetograms." *The Astrophysical Journal* 723.1 (2010): 634. [pdf @ researchgate](https://www.researchgate.net/profile/Todd_Hoeksema/publication/230964405_Testing_Automated_Solar_Flare_Forecasting_with_13_Years_of_Michelson_Doppler_Imager_Magnetograms/links/53e4fe2e0cf25d674e950c9d.pdf)

[5] Barnes, G., and K. D. Leka. "Evaluating the performance of solar flare forecasting methods." *The Astrophysical Journal Letters* 688.2 (2008): L107. [pdf @ solweb](http://solweb.oma.be/users/evarob/Literature/Papers/Solar%20Physics/2008%20Barnes%20and%20Leka%20Evaluation%20of%20solar%20flare%20forecasting%20methods.pdf)

----

#### Authors
* *Azim Ahmadzadeh* - [webpage](https://grid.cs.gsu.edu/~aahmadzadeh1/)
* *Berkay Aydin* - [webpage](https://grid.cs.gsu.edu/~baydin2/)


#### Institute
* [Georgia State University](https://www.gsu.edu/) > [Computer Science Department](https://www.cs.gsu.edu/) > [DMLab](http://dmlab.cs.gsu.edu/) 

#### License
This project is licensed under the GNU General Public License - see the [GPL LICENSE](http://www.gnu.org/licenses/gpl.html) for details.
